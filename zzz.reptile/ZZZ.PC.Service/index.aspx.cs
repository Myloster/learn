﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace ZZZ.PC.Service
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            //cookie是在要爬网页F12看到的cookie都复制粘贴到这里就可以
            string cookieStr = "PSTM=1537782422; BIDUPSID=2EF7F52B4522F4946D8F8361F1AF5227; userDeviceId=WkOeiJ1618941322; __yjs_duid=1_ea79a49e192e9e3e10c6fd5576e571611620436936554; H_WISE_SIDS=110085_127969_128698_131862_174662_175654_175756_176158_176555_176589_176678_177008_177093_177168_177224_177406_177749_177949_178328_178429_178500_178631_178711_178771_178797_178817_178895_178946_178956_178992_179014_179200_179330_179340_179350_179400_179401_179457_179521_179592_179645_179819_180099_180110_180116_180276_180300_180325_180565_180606; BDUSS=NEV3JRRDB5VWlHZXpLRHl2LWR0OFVwV1V1V2p-RlpyMW8tQWxzMnlNRFNnbFZoSVFBQUFBJCQAAAAAAAAAAAEAAABZq9cVODhoZWFydHNreQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANL1LWHS9S1hWm; BDUSS_BFESS=NEV3JRRDB5VWlHZXpLRHl2LWR0OFVwV1V1V2p-RlpyMW8tQWxzMnlNRFNnbFZoSVFBQUFBJCQAAAAAAAAAAAEAAABZq9cVODhoZWFydHNreQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANL1LWHS9S1hWm; MCITY=-%3A; BAIDUID=7E501E8BBD0ED60EE056F588322546A0:FG=1; BAIDUID_BFESS=7E501E8BBD0ED60EE056F588322546A0:FG=1; BAIDU_WISE_UID=wapp_1635409241826_531; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; H_PS_PSSID=34943_34444_34067_34584_34505_34917_34579_26350_34828_34868; BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0; delPer=0; PSINO=3; BA_HECTOR=2l2g8h85a5al04249h1gnuojj0r";

            //创建请求
            System.Net.HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://mbd.baidu.com/newspage/api/getpcvoicelist?callback=JSONP_0&");
            //请求方式
            httpWebRequest.Method = "GET";
            //设置请求超时时间
            httpWebRequest.Timeout = 20000;

            //设置cookie
            httpWebRequest.Headers.Add("Cookie", cookieStr);
            //发送请求
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //利用Stream流读取返回数据
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);
            //获得最终数据，一般是json
            string responseContent = streamReader.ReadToEnd();

            streamReader.Close();
            httpWebResponse.Close();

            jsondata.Text = responseContent;
        }
        protected void btnPost_Click(object sender, EventArgs e)
        {
            //参数列表
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("page", "0");
            parameters.Add("size", "12");
            parameters.Add("type", "0");
            parameters.Add("sceneIds", "406");

            //cookie是在要爬网页F12看到的cookie都复制粘贴到这里就可以
            string cookieStr = "53gid2=10793982404000; 53revisit=1624370597079; JSESSIONID=F8632F5708CF5D22FA7136CC0A0B8132; lyurl=www.jibai.com; ZU_PU_COOKIE_ID=4ee2fa3f-705c-4fb2-a1ea-e34e649773c2; Hm_lvt_70a212853abe34ebdbcfff457771955e=1635738664; visitor_type=old; 53gid0=10793982404000; 53gid1=10793982404000; 53kf_72389531_from_host=www.jibai.com; 53kf_72389531_keyword=https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DBVvWQTgnhJzTPdRSGZVTus01RZD19AEHAUcPRjMfp97%26wd%3D%26eqid%3Dea25280500012a6e00000005617f6418; 53kf_72389531_land_page=https%253A%252F%252Fwww.jibai.com%252F; kf_72389531_land_page_ok=1; 53uvid=1; onliner_zdfq72389531=0; invite_53kf_totalnum_1=1; my_acc_reauto_time=null; Hm_lpvt_70a212853abe34ebdbcfff457771955e=1635738681";

            //创建请求
            System.Net.HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://www.jibai.com/products");
            //请求方式
            request.Method = "POST";
            //设置请求超时时间
            request.Timeout = 20000;

            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";

            //request.Connection = "keep-alive";

            //设置cookie
            request.Headers.Add("Cookie", cookieStr);

            //设置发送参数
            byte[] postData = Encoding.UTF8.GetBytes(BuildQuery(parameters, "utf8"));   //使用utf-8格式组装post参数
            Stream reqStream = null;
            reqStream = request.GetRequestStream();
            reqStream.Write(postData, 0, postData.Length);

            //发送请求
            HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();

            //利用Stream流读取返回数据
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);

            //获得最终数据，一般是json
            string responseContent = streamReader.ReadToEnd();

            streamReader.Close();
            httpWebResponse.Close();

            jsondata.Text = responseContent;
        }

        //组装请求参数
        private string BuildQuery(IDictionary<string, string> parameters, string encode)
        {
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;
            IEnumerator<KeyValuePair<string, string>> dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                string name = dem.Current.Key;
                string value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name))
                {
                    if (hasParam)
                    {
                        postData.Append("&");
                    }
                    postData.Append(name);
                    postData.Append("=");
                    if (encode == "gb2312")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.GetEncoding("gb2312")));
                    }
                    else if (encode == "utf8")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    }
                    else
                    {
                        postData.Append(value);
                    }
                    hasParam = true;
                }
            }
            return postData.ToString();
        }

        //formdata请求方式
        protected void btn3ztb_Click(object sender, EventArgs e)
        {
            //Content-Type参数样式列表
            //multipart / form - data; boundary = ----WebKitFormBoundaryaMcEBhOSSj0VQBFy
            //multipart / form - data; boundary = ----WebKitFormBoundaryXGPIUzpv1BN7rZC4

            //生成boundary随机数据，拼接ContentType
            string boundary = "----WebKitFormBoundary" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15);

            //创建请求
            System.Net.HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://121.5.207.132:5000/api/Login/getlogin");

            //请求方式
            request.Method = "POST";
            //设置请求超时时间
            request.Timeout = 20000;
            request.ContentType = "multipart/form-data; boundary="+boundary;

            //设置formdata参数
            string formdata = "--"+ boundary+ "\r\n";
            formdata += "Content-Disposition: form-data; name=\"username\"" + "\r\n\r\n";
            formdata += "admin" + "\r\n";
            formdata += "--" + boundary + "\r\n";
            formdata += "Content-Disposition: form-data; name=\"password\"" + "\r\n\r\n";
            formdata += "admin789" + "\r\n";
            formdata += "--" + boundary + "--";

            //设置发送参数
            byte[] postData = Encoding.UTF8.GetBytes(formdata);
            Stream reqStream = null;
            reqStream = request.GetRequestStream();
            reqStream.Write(postData, 0, postData.Length);

            //发送请求
            HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();

            //利用Stream流读取返回数据
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);

            //获得最终数据，一般是json
            string responseContent = streamReader.ReadToEnd();

            streamReader.Close();
            httpWebResponse.Close();

            //JSON反序列化
            LoginMessage loginMessage = JsonConvert.DeserializeObject<LoginMessage>(responseContent);

            jsondata.Text = loginMessage.token;
        }

    }

    public class LoginMessage 
    {
        public string status { get; set; }
        public string islogin { get; set; }
        public string token { get; set; }

    }
}