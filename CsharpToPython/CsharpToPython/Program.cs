﻿using Python.Runtime;
using System;

namespace CsharpToPython
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Py.GIL())
            {
                Console.WriteLine("---------------------------");
                dynamic np = Py.Import("time");
                Console.WriteLine(np.localtime());
                Console.WriteLine("---------------------------");
                Console.ReadKey();
            }
        }
    }
}
